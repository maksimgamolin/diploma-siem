import json
from time import sleep

import requests

from logs import FailedLoginLog, CdromLogs, UsbDiskLogs, NetworkLog

LOGS_LISTENERS = [
    FailedLoginLog(),
    CdromLogs(),
    UsbDiskLogs(),
    NetworkLog()
]
NAME = "home-desktop"
PATH = "http://192.168.220.139:8000/monitoring/add/"


class Watcher:

    def collect_logs(self):
        logs = {}
        for listener in LOGS_LISTENERS:
            logs[listener.name] = listener.logs()
        return logs

    def run(self):
        while True:
            logs = self.collect_logs()
            payload = {
                "name": NAME,
                "logs": logs
            }
            headers = {'content-type': 'application/json'}
            # from pprint import pprint; pprint(payload)
            try:
                requests.post(PATH, data=json.dumps(payload), headers=headers)
            except Exception:
                pass
            sleep(1)


if __name__ == '__main__':
    watcher = Watcher()
    watcher.run()
