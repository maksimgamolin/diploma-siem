#  sudo apt-get install scanlogd
#  scanlogd
#  Для сканирования nmap -p 1-65535 -T4 -A -v 192.168.1.148
import logging
import subprocess

from logs.base import BaseLog

logger = logging.getLogger(__name__)


class NetworkLog(BaseLog):
    name = 'network'

    def logs(self):
        try:
            a = subprocess.check_output("cat /var/log/syslog | grep scanlogd",
                                        shell=True)  # в строке есть authentication failure
            return a.decode('utf-8').split('\n')
        except Exception:
            logger.exception(self.name)
            return []
