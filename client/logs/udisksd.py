import logging
import subprocess

from logs.base import BaseLog

logger = logging.getLogger(__name__)


class CdromLogs(BaseLog):
    name = 'cd_rom'

    def logs(self):
        try:
            a = subprocess.check_output("cat /var/log/syslog | grep 'udisksd'", shell=True)  # в строке есть Mounted
            return a.decode('utf-8').split('\n')
        except Exception:
            logger.exception(self.name)
            return []
