from logs.faled_logins import FailedLoginLog
from logs.network import NetworkLog
from logs.udisksd import CdromLogs
from logs.usb_storage import UsbDiskLogs
