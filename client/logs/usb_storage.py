import logging
import subprocess

from logs.base import BaseLog

logger = logging.getLogger(__name__)


class UsbDiskLogs(BaseLog):
    name = 'usb_storage'

    def logs(self):
        try:
            a = subprocess.check_output("cat /var/log/syslog | grep 'usb-storage'", shell=True)
            return a.decode('utf-8').split('\n')
        except Exception:
            logger.exception(self.name)
            return []
