import logging
import subprocess

from logs.base import BaseLog

logger = logging.getLogger(__name__)


class FailedLoginLog(BaseLog):
    name = 'auth'

    def logs(self):
        try:
            a = subprocess.check_output("cat /var/log/auth.log | grep failure",
                                        shell=True)  # в строке есть authentication failure
            return a.decode('utf-8').split('\n')
        except Exception:
            logger.exception(self.name)
            return []
