# Register your models here. LogType PCLogItem
from django.contrib import admin

from monitoring.models import UserPC, LogType, PCLogItem, SecurityEvent
from monitoring.views import EventsDashboardAdminView

_admin_site_get_urls = admin.site.get_urls


def get_urls():
    from django.conf.urls import url
    urls = _admin_site_get_urls()
    urls += [
        url(r'^dashboard/$',
            admin.site.admin_view(EventsDashboardAdminView.as_view()))
    ]
    return urls


admin.site.get_urls = get_urls


class UserPCAdmin(admin.ModelAdmin):
    pass


class LogTypeAdmin(admin.ModelAdmin):
    pass


class PCLogItemAdmin(admin.ModelAdmin):
    list_filter = ['log_type']
    list_display = ['remote_registered_at', 'log_type', 'text']


class SecurityEventAdmin(admin.ModelAdmin):
    pass


admin.site.register(UserPC, UserPCAdmin)
admin.site.register(LogType, LogTypeAdmin)
admin.site.register(PCLogItem, PCLogItemAdmin)
admin.site.register(SecurityEvent, SecurityEventAdmin)
