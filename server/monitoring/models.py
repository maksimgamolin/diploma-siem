from django.db import models


class UserPC(models.Model):
    last_active_at = models.DateTimeField('Дата и время последней активности')
    is_compromised = models.BooleanField('Бали ли машина скомпрометирована', default=False)
    name = models.CharField(verbose_name='Имя машины', max_length=255)

    def __str__(self):
        return self.name


class LogType(models.Model):
    key = models.CharField('Ключ', max_length=255)

    def __str__(self):
        return self.key


class PCLogItem(models.Model):
    device = models.ForeignKey(UserPC, on_delete=models.CASCADE, verbose_name='Компьютер')
    created_at = models.DateTimeField('Дата создания записи', auto_now_add=True)
    remote_registered_at = models.DateTimeField('Дата/время регистрации на удаленном компьютере')
    log_type = models.ForeignKey(LogType, on_delete=models.CASCADE, verbose_name='Тип')
    text = models.TextField('Текст записи')

    def __str__(self):
        return f"{self.device.name} - {self.log_type.key} - {self.text[:250]}"


class SecurityEvent(models.Model):
    INFO = 0
    WARNING = 1
    CRITICAL = 2

    LEVEL_CHOICES = (
        (INFO, 'Информация'),
        (WARNING, 'Внимание'),
        (CRITICAL, 'Угроза')
    )

    level = models.PositiveSmallIntegerField(verbose_name='Уровень угрозы', choices=LEVEL_CHOICES)
    is_active = models.BooleanField(verbose_name='Активно ли?')
    records = models.ManyToManyField(PCLogItem, verbose_name='Записи логов')
    name = models.CharField('Короткое название', max_length=255)
    info = models.TextField('Описание события')
    created_at = models.DateTimeField(verbose_name='Дата создания', auto_now_add=True)

    def __str__(self):
        return self.name
