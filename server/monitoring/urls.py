from django.urls import path

from monitoring.views import AddLogView

urlpatterns = [
    path('add/', AddLogView.as_view())
]
