#
# curl -H "Content-Type: application/json" -X POST --data '{"name": "home-desktop","logs":{"auth": ["Apr 10 18:16:38 vmax-VirtualBox udisksd[538]: udisks daemon", "Apr 10 21:17:00 vmax-VirtualBox sshd[3686]: pam_unix(sshd:auth): "]}}' localhost:8000/monitoring/add/
import json
import re
from datetime import datetime, timedelta

from django.db.models import Count
from django.http.response import HttpResponse
from django.shortcuts import get_object_or_404
from django.views import View
from django.views.generic import TemplateView

from analytics.cases.insert_cd import InsertCDAnalytics
from analytics.cases.port_scanning import PortScanningAnalytic
from analytics.cases.ssh_brutforce import SSHBrutforceAnalytics
from monitoring.models import UserPC, LogType, PCLogItem, SecurityEvent

LOG_TIME_PATTERN = re.compile(r'^([a-zA-Z]+) ([0-9]+) ([0-9:]+) ([\s\S]+)')


class AddLogView(View):

    def post(self, request, *args, **kwargs):
        """
        {"name":"имя компютера",
        "logs": {
                "log-key": ["records",...]
            }
        """
        today = datetime.now()
        data = json.loads(request.body.decode('utf-8'))

        name = data.get('name')
        user_pc = get_object_or_404(UserPC, name=name)
        logs = data.get('logs')
        for log_name in logs.keys():
            log_type = get_object_or_404(LogType, key=log_name)
            for row in logs[log_name]:
                if not row:
                    continue
                row_items = LOG_TIME_PATTERN.findall(row)[0]
                date_of_row = datetime.strptime(f'{row_items[1]}-{row_items[0]}-{today.year} {row_items[2]}',
                                                '%d-%b-%Y %H:%M:%S')
                row_body = row_items[3]
                row_in_db, _ = PCLogItem.objects.get_or_create(
                    remote_registered_at=date_of_row,
                    text=row_body,
                    log_type=log_type,
                    device=user_pc
                )
        PortScanningAnalytic().run()
        InsertCDAnalytics().run()
        SSHBrutforceAnalytics().run()
        return HttpResponse('ok')


class EventsDashboardAdminView(TemplateView):
    template_name = 'events_dashboard.html'

    def events_grouped_by_day(self):
        return SecurityEvent.objects.extra(select={'day': 'date( created_at )'}).values('day') \
            .annotate(available=Count('created_at'))

    def events_by_level(self):
        return SecurityEvent.objects.values('level').annotate(count=Count('id')).order_by('level')

    def logs_by_type_and_day(self):
        start_date = datetime.now() - timedelta(days=7)
        return PCLogItem.objects \
            .filter(created_at__gte=start_date) \
            .extra(select={'day': 'date( created_at )'}) \
            .values('day', 'log_type__key') \
            .annotate(available=Count('id')) \
            .order_by('day')

    def format_logs(self, name, date_mask, logs):
        filtered_items = [i for i in logs if name == i['log_type__key']]
        print(filtered_items)
        result = []
        for day in date_mask:
            cnt = next((i['available'] for i in filtered_items if i['day'] == day), 0)
            result.append(cnt)
        return result

    def get_context_data(self, **kwargs):
        ctx = super(EventsDashboardAdminView, self).get_context_data(**kwargs)
        events_per_day = self.events_grouped_by_day()
        events_by_level = self.events_by_level()
        security_events_level_choices = dict(SecurityEvent.LEVEL_CHOICES)
        logs = sorted(self.logs_by_type_and_day(), key=lambda i: i['day'])
        print(logs)
        logs_days = sorted(list({i['day'] for i in logs}))
        ctx['ALL_EVENTS_DAYS'] = json.dumps([i['day'] for i in events_per_day])
        ctx['ALL_EVENTS_VALUES'] = json.dumps([i['available'] for i in events_per_day])
        ctx['ALL_EVENTS_BY_TYPE_NAMES'] = json.dumps(
            [security_events_level_choices[i['level']] for i in events_by_level])
        ctx['ALL_EVENTS_BY_TYPE_VALUES'] = json.dumps([i['count'] for i in events_by_level])
        ctx['ALL_LOGS_DAYS'] = json.dumps(logs_days)
        ctx['ALL_LOGS_AUTH'] = self.format_logs('auth', logs_days, logs)
        ctx['ALL_LOGS_CD_ROM'] = self.format_logs('cd_rom', logs_days, logs)
        ctx['ALL_LOGS_USB_STORAGE'] = self.format_logs('usb_storage', logs_days, logs)
        ctx['ALL_LOGS_NETWORK'] = self.format_logs('network', logs_days, logs)
        ctx['EVENTS'] = SecurityEvent.objects.filter(is_active=True).order_by('-created_at')
        return ctx
