from analytics.cases.base import BaseAnalytic
from monitoring.models import SecurityEvent


class InsertCDAnalytics(BaseAnalytic):
    SCAN_REGEXP = r"Mounted"

    def find_port_scan(self, log_items):
        return log_items.filter(
            log_type__key='cd_rom',
            text__contains=self.SCAN_REGEXP  # todo переделать на регулярку
        )

    def run(self):
        used_log_items_ids = self.ids_of_used_logitems()
        unused_log_items = self.get_unused_log_items(used_log_items_ids)
        filtered_for_row_scan = self.find_port_scan(unused_log_items)
        if not filtered_for_row_scan.exists():
            return
        event = SecurityEvent.objects.create(
            level=SecurityEvent.WARNING,
            is_active=True,
            name='Вставлен CD диск',
            info='Пользователь вставил диск в компьютер'
        )
        event.records.add(*filtered_for_row_scan)
