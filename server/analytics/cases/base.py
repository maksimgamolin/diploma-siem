from datetime import datetime, timedelta

from monitoring.models import SecurityEvent, PCLogItem


class BaseAnalytic:

    def ids_of_used_logitems(self):
        return tuple(SecurityEvent.records.through.objects.values_list('pclogitem_id', flat=True))

    def get_unused_log_items(self, ids):
        start_date = datetime.now() - timedelta(days=1)
        items = PCLogItem.objects.filter(
            created_at__gte=start_date
        ).exclude(
            id__in=ids
        )
        return items

    def run(self):
        pass
