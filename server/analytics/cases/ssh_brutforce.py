from analytics.cases.base import BaseAnalytic
from monitoring.models import SecurityEvent


class SSHBrutforceAnalytics(BaseAnalytic):
    SCAN_REGEXP = r"[\s\S]+sshd[\s\S]+authentication failures;[\s\S]+"

    def find_ssh_brutforce_rows(self, log_items):
        return log_items.filter(
            log_type__key='auth',
            text__regex=self.SCAN_REGEXP
        )

    def run(self):
        used_log_items_ids = self.ids_of_used_logitems()
        unused_log_items = self.get_unused_log_items(used_log_items_ids)
        brutforce_rows = self.find_ssh_brutforce_rows(unused_log_items)
        if brutforce_rows.count() >= 2:
            event = SecurityEvent.objects.create(
                level=SecurityEvent.CRITICAL,
                is_active=True,
                name='Возможная брутфорс атака по ssh',
                info='Возможная брутфорс атака по ssh'
            )
            event.records.add(*brutforce_rows)
