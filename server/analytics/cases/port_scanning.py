from analytics.cases.base import BaseAnalytic
from monitoring.models import SecurityEvent


class PortScanningAnalytic(BaseAnalytic):
    SCAN_REGEXP = r"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3} to \d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3} ports"

    def find_port_scan(self, log_items):
        return log_items.filter(
            log_type__key='network',
            text__regex=self.SCAN_REGEXP
        )

    def run(self):
        used_log_items_ids = self.ids_of_used_logitems()
        unused_log_items = self.get_unused_log_items(used_log_items_ids)
        filtered_for_row_scan = self.find_port_scan(unused_log_items)
        if not filtered_for_row_scan.exists():
            return
        event = SecurityEvent.objects.create(
            level=SecurityEvent.CRITICAL,
            is_active=True,
            name='Сканирование портов',
            info='Обнаружено сканирование портов'
        )
        event.records.add(*filtered_for_row_scan)
